use tracker;
use gio;

//SparqlCursor

fn main() {
    println!("Hello, world!");

    let connection = tracker::SparqlConnection::bus_new(
            "org.freedesktop.Tracker3.Miner.Files", None, None)
            .unwrap();

    let query1 = "SELECT nie:url(?u) WHERE { ?u a nfo:FileDataObject }";
    let cursor = connection.query(query1, None).unwrap();

    let mut i = 0;
    while cursor.next(None) == Ok(()) {
        let (string, _) = cursor.get_string(i);
        println!("Result [{}]: {}", i, string.as_str());
    }

    println!("A total of '{}' results were found", i);
}
