// This file was generated by gir (https://github.com/gtk-rs/gir @ 6123d59)
// from gir-files (https://github.com/gtk-rs/gir-files @ d68c396)
// DO NOT EDIT

use std::env;
use std::error::Error;
use std::path::Path;
use std::mem::{align_of, size_of};
use std::process::Command;
use std::str;
use tempfile::Builder;
use tracker_sys::*;

static PACKAGES: &[&str] = &["tracker-sparql-3.0"];

#[derive(Clone, Debug)]
struct Compiler {
    pub args: Vec<String>,
}

impl Compiler {
    pub fn new() -> Result<Compiler, Box<dyn Error>> {
        let mut args = get_var("CC", "cc")?;
        args.push("-Wno-deprecated-declarations".to_owned());
        // For %z support in printf when using MinGW.
        args.push("-D__USE_MINGW_ANSI_STDIO".to_owned());
        args.extend(get_var("CFLAGS", "")?);
        args.extend(get_var("CPPFLAGS", "")?);
        args.extend(pkg_config_cflags(PACKAGES)?);
        Ok(Compiler { args })
    }

    pub fn define<'a, V: Into<Option<&'a str>>>(&mut self, var: &str, val: V) {
        let arg = match val.into() {
            None => format!("-D{}", var),
            Some(val) => format!("-D{}={}", var, val),
        };
        self.args.push(arg);
    }

    pub fn compile(&self, src: &Path, out: &Path) -> Result<(), Box<dyn Error>> {
        let mut cmd = self.to_command();
        cmd.arg(src);
        cmd.arg("-o");
        cmd.arg(out);
        let status = cmd.spawn()?.wait()?;
        if !status.success() {
            return Err(format!("compilation command {:?} failed, {}",
                               &cmd, status).into());
        }
        Ok(())
    }

    fn to_command(&self) -> Command {
        let mut cmd = Command::new(&self.args[0]);
        cmd.args(&self.args[1..]);
        cmd
    }
}

fn get_var(name: &str, default: &str) -> Result<Vec<String>, Box<dyn Error>> {
    match env::var(name) {
        Ok(value) => Ok(shell_words::split(&value)?),
        Err(env::VarError::NotPresent) => Ok(shell_words::split(default)?),
        Err(err) => Err(format!("{} {}", name, err).into()),
    }
}

fn pkg_config_cflags(packages: &[&str]) -> Result<Vec<String>, Box<dyn Error>> {
    if packages.is_empty() {
        return Ok(Vec::new());
    }
    let mut cmd = Command::new("pkg-config");
    cmd.arg("--cflags");
    cmd.args(packages);
    let out = cmd.output()?;
    if !out.status.success() {
        return Err(format!("command {:?} returned {}",
                           &cmd, out.status).into());
    }
    let stdout = str::from_utf8(&out.stdout)?;
    Ok(shell_words::split(stdout.trim())?)
}


#[derive(Copy, Clone, Debug, Eq, PartialEq)]
struct Layout {
    size: usize,
    alignment: usize,
}

#[derive(Copy, Clone, Debug, Default, Eq, PartialEq)]
struct Results {
    /// Number of successfully completed tests.
    passed: usize,
    /// Total number of failed tests (including those that failed to compile).
    failed: usize,
    /// Number of tests that failed to compile.
    failed_to_compile: usize,
}

impl Results {
    fn record_passed(&mut self) {
        self.passed += 1;
    }
    fn record_failed(&mut self) {
        self.failed += 1;
    }
    fn record_failed_to_compile(&mut self) {
        self.failed += 1;
        self.failed_to_compile += 1;
    }
    fn summary(&self) -> String {
        format!(
            "{} passed; {} failed (compilation errors: {})",
            self.passed,
            self.failed,
            self.failed_to_compile)
    }
    fn expect_total_success(&self) {
        if self.failed == 0 {
            println!("OK: {}", self.summary());
        } else {
            panic!("FAILED: {}", self.summary());
        };
    }
}

#[test]
fn cross_validate_constants_with_c() {
    let tmpdir = Builder::new().prefix("abi").tempdir().expect("temporary directory");
    let cc = Compiler::new().expect("configured compiler");

    assert_eq!("1",
               get_c_value(tmpdir.path(), &cc, "1").expect("C constant"),
               "failed to obtain correct constant value for 1");

    let mut results : Results = Default::default();
    for (i, &(name, rust_value)) in RUST_CONSTANTS.iter().enumerate() {
        match get_c_value(tmpdir.path(), &cc, name) {
            Err(e) => {
                results.record_failed_to_compile();
                eprintln!("{}", e);
            },
            Ok(ref c_value) => {
                if rust_value == c_value {
                    results.record_passed();
                } else {
                    results.record_failed();
                    eprintln!("Constant value mismatch for {}\nRust: {:?}\nC:    {:?}",
                              name, rust_value, c_value);
                }
            }
        };
        if (i + 1) % 25 == 0 {
            println!("constants ... {}", results.summary());
        }
    }
    results.expect_total_success();
}

#[test]
fn cross_validate_layout_with_c() {
    let tmpdir = Builder::new().prefix("abi").tempdir().expect("temporary directory");
    let cc = Compiler::new().expect("configured compiler");

    assert_eq!(Layout {size: 1, alignment: 1},
               get_c_layout(tmpdir.path(), &cc, "char").expect("C layout"),
               "failed to obtain correct layout for char type");

    let mut results : Results = Default::default();
    for (i, &(name, rust_layout)) in RUST_LAYOUTS.iter().enumerate() {
        match get_c_layout(tmpdir.path(), &cc, name) {
            Err(e) => {
                results.record_failed_to_compile();
                eprintln!("{}", e);
            },
            Ok(c_layout) => {
                if rust_layout == c_layout {
                    results.record_passed();
                } else {
                    results.record_failed();
                    eprintln!("Layout mismatch for {}\nRust: {:?}\nC:    {:?}",
                              name, rust_layout, &c_layout);
                }
            }
        };
        if (i + 1) % 25 == 0 {
            println!("layout    ... {}", results.summary());
        }
    }
    results.expect_total_success();
}

fn get_c_layout(dir: &Path, cc: &Compiler, name: &str) -> Result<Layout, Box<dyn Error>> {
    let exe = dir.join("layout");
    let mut cc = cc.clone();
    cc.define("ABI_TYPE_NAME", name);
    cc.compile(Path::new("tests/layout.c"), &exe)?;

    let mut abi_cmd = Command::new(exe);
    let output = abi_cmd.output()?;
    if !output.status.success() {
        return Err(format!("command {:?} failed, {:?}",
                           &abi_cmd, &output).into());
    }

    let stdout = str::from_utf8(&output.stdout)?;
    let mut words = stdout.trim().split_whitespace();
    let size = words.next().unwrap().parse().unwrap();
    let alignment = words.next().unwrap().parse().unwrap();
    Ok(Layout {size, alignment})
}

fn get_c_value(dir: &Path, cc: &Compiler, name: &str) -> Result<String, Box<dyn Error>> {
    let exe = dir.join("constant");
    let mut cc = cc.clone();
    cc.define("ABI_CONSTANT_NAME", name);
    cc.compile(Path::new("tests/constant.c"), &exe)?;

    let mut abi_cmd = Command::new(exe);
    let output = abi_cmd.output()?;
    if !output.status.success() {
        return Err(format!("command {:?} failed, {:?}",
                           &abi_cmd, &output).into());
    }

    let output = str::from_utf8(&output.stdout)?.trim();
    if !output.starts_with("###gir test###") ||
       !output.ends_with("###gir test###") {
        return Err(format!("command {:?} return invalid output, {:?}",
                           &abi_cmd, &output).into());
    }

    Ok(String::from(&output[14..(output.len() - 14)]))
}

const RUST_LAYOUTS: &[(&str, Layout)] = &[
    ("TrackerEndpoint", Layout {size: size_of::<TrackerEndpoint>(), alignment: align_of::<TrackerEndpoint>()}),
    ("TrackerNamespaceManagerClass", Layout {size: size_of::<TrackerNamespaceManagerClass>(), alignment: align_of::<TrackerNamespaceManagerClass>()}),
    ("TrackerNotifier", Layout {size: size_of::<TrackerNotifier>(), alignment: align_of::<TrackerNotifier>()}),
    ("TrackerNotifierEventType", Layout {size: size_of::<TrackerNotifierEventType>(), alignment: align_of::<TrackerNotifierEventType>()}),
    ("TrackerResource", Layout {size: size_of::<TrackerResource>(), alignment: align_of::<TrackerResource>()}),
    ("TrackerSparqlConnection", Layout {size: size_of::<TrackerSparqlConnection>(), alignment: align_of::<TrackerSparqlConnection>()}),
    ("TrackerSparqlConnectionFlags", Layout {size: size_of::<TrackerSparqlConnectionFlags>(), alignment: align_of::<TrackerSparqlConnectionFlags>()}),
    ("TrackerSparqlCursor", Layout {size: size_of::<TrackerSparqlCursor>(), alignment: align_of::<TrackerSparqlCursor>()}),
    ("TrackerSparqlError", Layout {size: size_of::<TrackerSparqlError>(), alignment: align_of::<TrackerSparqlError>()}),
    ("TrackerSparqlStatement", Layout {size: size_of::<TrackerSparqlStatement>(), alignment: align_of::<TrackerSparqlStatement>()}),
    ("TrackerSparqlValueType", Layout {size: size_of::<TrackerSparqlValueType>(), alignment: align_of::<TrackerSparqlValueType>()}),
];

const RUST_CONSTANTS: &[(&str, &str)] = &[
    ("(gint) TRACKER_NOTIFIER_EVENT_CREATE", "0"),
    ("(gint) TRACKER_NOTIFIER_EVENT_DELETE", "1"),
    ("(gint) TRACKER_NOTIFIER_EVENT_UPDATE", "2"),
    ("TRACKER_PREFIX_DC", "http://purl.org/dc/elements/1.1/"),
    ("TRACKER_PREFIX_MFO", "http://tracker.api.gnome.org/ontology/v3/mfo#"),
    ("TRACKER_PREFIX_NAO", "http://tracker.api.gnome.org/ontology/v3/nao#"),
    ("TRACKER_PREFIX_NCO", "http://tracker.api.gnome.org/ontology/v3/nco#"),
    ("TRACKER_PREFIX_NFO", "http://tracker.api.gnome.org/ontology/v3/nfo#"),
    ("TRACKER_PREFIX_NIE", "http://tracker.api.gnome.org/ontology/v3/nie#"),
    ("TRACKER_PREFIX_NMM", "http://tracker.api.gnome.org/ontology/v3/nmm#"),
    ("TRACKER_PREFIX_NRL", "http://tracker.api.gnome.org/ontology/v3/nrl#"),
    ("TRACKER_PREFIX_OSINFO", "http://tracker.api.gnome.org/ontology/v3/osinfo#"),
    ("TRACKER_PREFIX_RDF", "http://www.w3.org/1999/02/22-rdf-syntax-ns#"),
    ("TRACKER_PREFIX_RDFS", "http://www.w3.org/2000/01/rdf-schema#"),
    ("TRACKER_PREFIX_SLO", "http://tracker.api.gnome.org/ontology/v3/slo#"),
    ("TRACKER_PREFIX_TRACKER", "http://tracker.api.gnome.org/ontology/v3/tracker#"),
    ("TRACKER_PREFIX_XSD", "http://www.w3.org/2001/XMLSchema#"),
    ("(guint) TRACKER_SPARQL_CONNECTION_FLAGS_FTS_ENABLE_STEMMER", "2"),
    ("(guint) TRACKER_SPARQL_CONNECTION_FLAGS_FTS_ENABLE_STOP_WORDS", "8"),
    ("(guint) TRACKER_SPARQL_CONNECTION_FLAGS_FTS_ENABLE_UNACCENT", "4"),
    ("(guint) TRACKER_SPARQL_CONNECTION_FLAGS_FTS_IGNORE_NUMBERS", "16"),
    ("(guint) TRACKER_SPARQL_CONNECTION_FLAGS_NONE", "0"),
    ("(guint) TRACKER_SPARQL_CONNECTION_FLAGS_READONLY", "1"),
    ("(gint) TRACKER_SPARQL_ERROR_CONSTRAINT", "0"),
    ("(gint) TRACKER_SPARQL_ERROR_INTERNAL", "1"),
    ("(gint) TRACKER_SPARQL_ERROR_NO_SPACE", "2"),
    ("(gint) TRACKER_SPARQL_ERROR_ONTOLOGY_NOT_FOUND", "3"),
    ("(gint) TRACKER_SPARQL_ERROR_OPEN_ERROR", "4"),
    ("(gint) TRACKER_SPARQL_ERROR_PARSE", "5"),
    ("(gint) TRACKER_SPARQL_ERROR_QUERY_FAILED", "6"),
    ("(gint) TRACKER_SPARQL_ERROR_TYPE", "7"),
    ("(gint) TRACKER_SPARQL_ERROR_UNKNOWN_CLASS", "8"),
    ("(gint) TRACKER_SPARQL_ERROR_UNKNOWN_GRAPH", "9"),
    ("(gint) TRACKER_SPARQL_ERROR_UNKNOWN_PROPERTY", "10"),
    ("(gint) TRACKER_SPARQL_ERROR_UNSUPPORTED", "11"),
    ("(gint) TRACKER_SPARQL_N_ERRORS", "12"),
    ("(gint) TRACKER_SPARQL_VALUE_TYPE_BLANK_NODE", "6"),
    ("(gint) TRACKER_SPARQL_VALUE_TYPE_BOOLEAN", "7"),
    ("(gint) TRACKER_SPARQL_VALUE_TYPE_DATETIME", "5"),
    ("(gint) TRACKER_SPARQL_VALUE_TYPE_DOUBLE", "4"),
    ("(gint) TRACKER_SPARQL_VALUE_TYPE_INTEGER", "3"),
    ("(gint) TRACKER_SPARQL_VALUE_TYPE_STRING", "2"),
    ("(gint) TRACKER_SPARQL_VALUE_TYPE_UNBOUND", "0"),
    ("(gint) TRACKER_SPARQL_VALUE_TYPE_URI", "1"),
];


