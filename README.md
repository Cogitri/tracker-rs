# Tracker-rs for Tracker 3.0
The only [Tracker](https://gnome.pages.gitlab.gnome.org/tracker/docs/developer/) documentation that contains the API for version 3 is the [bleeding edge docummentation](https://gnome.pages.gitlab.gnome.org/tracker/docs/api-preview/libtracker-sparql-3/).

All the functions that [GIR](https://github.com/gtk-rs/gir) can simply generate are available. These are the functions available in Tracker 3.0, while the documentation contains newer versions of Tracker. i.e. TrackerBatch is not in tracker-rs.

The class `EndpointDBus` is not available. \
The function `foreach` in `NamespaceManager` is not available. \
The function `get_values` in `Resource` is not available. \
The function `update_array_async` in `SparqlConnection` is not available.

# Work In Progress
- Write examples to check the basic functionality is working.
- Test flatpak intergration and async functionality.

I do not know enough of Rust, C, or FFI to enable the class and functions above, I will try in the future. Please contribute to enable everything in Tracker 3.x, writing the functions in idiomatic rust, and creating working examples. Also a suite of tests would be awesome! 
