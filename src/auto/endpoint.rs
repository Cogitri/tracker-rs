// This file was generated by gir (https://github.com/gtk-rs/gir)
// from gir-files (https://github.com/gtk-rs/gir-files)
// DO NOT EDIT

use crate::SparqlConnection;
use glib::object::IsA;
use glib::translate::*;
use std::fmt;

glib::wrapper! {
    pub struct Endpoint(Object<ffi::TrackerEndpoint, ffi::TrackerEndpointClass>);

    match fn {
        get_type => || ffi::tracker_endpoint_get_type(),
    }
}

pub const NONE_ENDPOINT: Option<&Endpoint> = None;

pub trait EndpointExt: 'static {
    #[doc(alias = "tracker_endpoint_get_sparql_connection")]
    fn get_sparql_connection(&self) -> Option<SparqlConnection>;
}

impl<O: IsA<Endpoint>> EndpointExt for O {
    fn get_sparql_connection(&self) -> Option<SparqlConnection> {
        unsafe {
            from_glib_none(ffi::tracker_endpoint_get_sparql_connection(self.as_ref().to_glib_none().0))
        }
    }
}

impl fmt::Display for Endpoint {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("Endpoint")
    }
}
