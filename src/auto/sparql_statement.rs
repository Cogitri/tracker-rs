// This file was generated by gir (https://github.com/gtk-rs/gir)
// from gir-files (https://github.com/gtk-rs/gir-files)
// DO NOT EDIT

use crate::SparqlConnection;
use crate::SparqlCursor;
use glib::object::IsA;
use glib::translate::*;
use std::boxed::Box as Box_;
use std::fmt;
use std::pin::Pin;
use std::ptr;

glib::wrapper! {
    pub struct SparqlStatement(Object<ffi::TrackerSparqlStatement, ffi::TrackerSparqlStatementClass>);

    match fn {
        get_type => || ffi::tracker_sparql_statement_get_type(),
    }
}

impl SparqlStatement {
    #[doc(alias = "tracker_sparql_statement_bind_boolean")]
    pub fn bind_boolean(&self, name: &str, value: bool) {
        unsafe {
            ffi::tracker_sparql_statement_bind_boolean(self.to_glib_none().0, name.to_glib_none().0, value.to_glib());
        }
    }

    #[doc(alias = "tracker_sparql_statement_bind_double")]
    pub fn bind_double(&self, name: &str, value: f64) {
        unsafe {
            ffi::tracker_sparql_statement_bind_double(self.to_glib_none().0, name.to_glib_none().0, value);
        }
    }

    #[doc(alias = "tracker_sparql_statement_bind_int")]
    pub fn bind_int(&self, name: &str, value: i64) {
        unsafe {
            ffi::tracker_sparql_statement_bind_int(self.to_glib_none().0, name.to_glib_none().0, value);
        }
    }

    #[doc(alias = "tracker_sparql_statement_bind_string")]
    pub fn bind_string(&self, name: &str, value: &str) {
        unsafe {
            ffi::tracker_sparql_statement_bind_string(self.to_glib_none().0, name.to_glib_none().0, value.to_glib_none().0);
        }
    }

    #[doc(alias = "tracker_sparql_statement_clear_bindings")]
    pub fn clear_bindings(&self) {
        unsafe {
            ffi::tracker_sparql_statement_clear_bindings(self.to_glib_none().0);
        }
    }

    #[doc(alias = "tracker_sparql_statement_execute")]
    pub fn execute<P: IsA<gio::Cancellable>>(&self, cancellable: Option<&P>) -> Result<SparqlCursor, glib::Error> {
        unsafe {
            let mut error = ptr::null_mut();
            let ret = ffi::tracker_sparql_statement_execute(self.to_glib_none().0, cancellable.map(|p| p.as_ref()).to_glib_none().0, &mut error);
            if error.is_null() { Ok(from_glib_full(ret)) } else { Err(from_glib_full(error)) }
        }
    }

    #[doc(alias = "tracker_sparql_statement_execute_async")]
    pub fn execute_async<P: IsA<gio::Cancellable>, Q: FnOnce(Result<SparqlCursor, glib::Error>) + Send + 'static>(&self, cancellable: Option<&P>, callback: Q) {
        let user_data: Box_<Q> = Box_::new(callback);
        unsafe extern "C" fn execute_async_trampoline<Q: FnOnce(Result<SparqlCursor, glib::Error>) + Send + 'static>(_source_object: *mut glib::gobject_ffi::GObject, res: *mut gio::ffi::GAsyncResult, user_data: glib::ffi::gpointer) {
            let mut error = ptr::null_mut();
            let ret = ffi::tracker_sparql_statement_execute_finish(_source_object as *mut _, res, &mut error);
            let result = if error.is_null() { Ok(from_glib_full(ret)) } else { Err(from_glib_full(error)) };
            let callback: Box_<Q> = Box_::from_raw(user_data as *mut _);
            callback(result);
        }
        let callback = execute_async_trampoline::<Q>;
        unsafe {
            ffi::tracker_sparql_statement_execute_async(self.to_glib_none().0, cancellable.map(|p| p.as_ref()).to_glib_none().0, Some(callback), Box_::into_raw(user_data) as *mut _);
        }
    }

    
    pub fn execute_async_future(&self) -> Pin<Box_<dyn std::future::Future<Output = Result<SparqlCursor, glib::Error>> + 'static>> {

        Box_::pin(gio::GioFuture::new(self, move |obj, send| {
            let cancellable = gio::Cancellable::new();
            obj.execute_async(
                Some(&cancellable),
                move |res| {
                    send.resolve(res);
                },
            );

            cancellable
        }))
    }

    #[doc(alias = "tracker_sparql_statement_get_connection")]
    pub fn get_connection(&self) -> Option<SparqlConnection> {
        unsafe {
            from_glib_none(ffi::tracker_sparql_statement_get_connection(self.to_glib_none().0))
        }
    }

    #[doc(alias = "tracker_sparql_statement_get_sparql")]
    pub fn get_sparql(&self) -> Option<glib::GString> {
        unsafe {
            from_glib_none(ffi::tracker_sparql_statement_get_sparql(self.to_glib_none().0))
        }
    }
}

impl fmt::Display for SparqlStatement {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("SparqlStatement")
    }
}
