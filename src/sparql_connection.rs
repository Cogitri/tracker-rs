use glib::object::IsA;
use glib::translate::*;
use sparql_connection;

pub trait SparqlConnectionExtManual: 'static {
    pub fn get_values(&self, property_uri: &str) -> Vec<glib::Value>;
}

impl<O: IsA<SparqlConnection>> RegionExtManual for O {
    #[doc(alias = "tracker_sparql_connection_update_array_async")]
    fn update_array_async<P: IsA<gio::Cancellable>, Q: FnOnce(Result<(), glib::Error>) + Send + 'static>(&self, sparql: &str, cancellable: Option<&P>, callback: Q) {
        let sparql_length = sparql.len() as i32;
        let user_data: Box_<Q> = Box_::new(callback);
        unsafe extern "C" fn update_array_async_trampoline<Q: FnOnce(Result<(), glib::Error>) + Send + 'static>(_source_object: *mut glib::gobject_ffi::GObject, res: *mut gio::ffi::GAsyncResult, user_data: glib::ffi::gpointer) {
            let mut error = ptr::null_mut();
            let _ = ffi::tracker_sparql_connection_update_array_finish(_source_object as *mut _, res, &mut error);
            let result = if error.is_null() { Ok(()) } else { Err(from_glib_full(error)) };
            let callback: Box_<Q> = Box_::from_raw(user_data as *mut _);
            callback(result);
        }
        let callback = update_array_async_trampoline::<Q>;
        unsafe {
            ffi::tracker_sparql_connection_update_array_async(self.to_glib_none().0, sparql.to_glib_none().0, sparql_length, cancellable.map(|p| p.as_ref()).to_glib_none().0, Some(callback), Box_::into_raw(user_data) as *mut _);
        }
    }
}

/*
error[E0277]: the trait bound `str: glib::translate::ToGlibPtr<'_, *mut *mut i8>` is not satisfied
   --> src/auto/sparql_connection.rs:181:93
    |
181 | ..._none().0, sparql.to_glib_none().0, sparql_length, cancellable.map(|p| p.as_ref()).to_glib_none().0, Some(callback), Box_::into_raw(us...
    |                      ^^^^^^^^^^^^ the trait `glib::translate::ToGlibPtr<'_, *mut *mut i8>` is not implemented for `str`
    |
    = help: the following implementations were found:
              <str as glib::translate::ToGlibPtr<'a, *const i8>>
              <str as glib::translate::ToGlibPtr<'a, *mut i8>>
*/
